﻿using System;

namespace toxi.math
{
    /**
     * Implementation of the circular interpolation function.
     * 
     * i = a-(b-a) * (sqrt(1 - (1 - f) * (1 - f) ))
     */
    class CircularInterpolation : IInterpolateStrategy
    {
        protected bool m_isFlipped;
        
        public CircularInterpolation() : this (false)
        {
            
        }

        /**
         * The interpolation slope can be flipped to have its steepest ascent
         * towards the end value, rather than at the beginning in the default
         * configuration.
         * 
         * @param isFlipped
         *            true, if slope is inverted
         */
        public CircularInterpolation(bool isFlipped)
        {
            this.m_isFlipped = isFlipped;
        }
        
        public float interpolate(float a, float b, float f)
        {
            if (m_isFlipped)
            {
                return a - (b - a) * ((float)Math.Sqrt(1 - f * f) - 1);
            }
            else
            {
                f = 1 - f;
                return a + (b - a) * ((float)Math.Sqrt(1 - f * f));
            }
        }

        public void setFlipped(bool isFlipped)
        {
            this.m_isFlipped = isFlipped;
        }
    }
}
