﻿using System;

namespace toxi.math
{
    /**
     * Implementation of the cosine interpolation function:
     * 
     * i = b+(a-b)*(0.5+0.5*cos(f*PI))
     */
    class CosineInterpolation : IInterpolateStrategy
    {
        /*
         * (non-Javadoc)
         * 
         */
        public float interpolate(float a, float b, float f)
        {
            return b + (a - b) * (float)(0.5 + 0.5 * Math.Cos(f * Math.PI));
        }
    }
}
