﻿namespace toxi.math
{
    /**
     * Defines a generic function to interpolate 2 float values.
     */
    interface IInterpolateStrategy
    {
        /**
         * Implements an interpolation equation using float values.
         * 
         * @param a
         *            current value
         * @param b
         *            target value
         * @param f
         *            normalized interpolation factor (0.0 .. 1.0)
         * @return interpolated value
         */
        float interpolate(float a, float b, float f);
    }
}
