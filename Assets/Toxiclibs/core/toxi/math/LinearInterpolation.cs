﻿namespace toxi.math
{
    /**
     * Implementation of the linear interpolation function
     * 
     * i = a + ( b - a ) * f
     */
    class LinearInterpolation : IInterpolateStrategy
    {
        public float interpolate(float a, float b, float f)
        {
            return a + (b - a) * f;
        }
    }
}
