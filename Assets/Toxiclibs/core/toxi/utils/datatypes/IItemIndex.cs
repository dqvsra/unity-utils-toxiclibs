﻿
using System.Collections.Generic;

namespace toxi.utils.datatypes
{
    interface IItemIndex<T>
    {
         void clear();

         T forID(int id);

         int getID(T item);

         List<T> getItems();

         int index(T item);

         bool isIndexed(T item);

         int reindex(T item, T newItem);

         int size();

         int unindex(T item);
    }
}
