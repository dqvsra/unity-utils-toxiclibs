﻿using System;
using System.Collections.Generic;
using toxi.math;

namespace toxi.utils.datatypes.ranges
{
    class DoubleRange
    {
        public double min, max;
        public double currValue;

        protected Random random = new Random();

        public DoubleRange(): this(0d, 1d)
        {
        }

        public DoubleRange(double min, double max)
        {
            // swap if necessary...
            if (min > max)
            {
                double t = max;
                max = min;
                min = t;
            }
            this.min = min;
            this.max = max;
            this.currValue = min;
        }

        public double adjustCurrentBy(double val)
        {
            return setCurrent(currValue + val);
        }

        public DoubleRange copy()
        {
            DoubleRange range = new DoubleRange(min, max);
            range.currValue = currValue;
            range.random = random;
            return range;
        }
        /**
        * Returns the value at the normalized position <code>(0.0 = min ... 1.0 =
        * max-EPS)</code> within the range. Since the max value is exclusive, the
        * value returned for position 1.0 is the range max value minus
        * {@link MathUtils#EPS}. Also note the given position is not being clipped
        * to the 0.0-1.0 interval, so when passing in values outside that interval
        * will produce out-of-range values too.
        * 
        * @param perc
        * @return value within the range
        */
        public double getAt(double perc)
        {
            return min + (max - min - MathUtils.EPS) * perc;
        }

        public double getCurrent()
        {
            return currValue;
        }

        public double getMedian()
        {
            return (min + max) * 0.5f;
        }

        public double getRange()
        {
            return max - min;
        }

        public bool isValueInRange(float val)
        {
            return val >= min && val <= max;
        }

        public double pickRandom()
        {
            currValue = MathUtils.random(random, (float)min, (float)max);
            return currValue;
        }

        public DoubleRange seed(int seed)
        {
            random = new Random(seed);
            return this;
        }

        public double setCurrent(double val)
        {
            currValue = MathUtils.clip(val, min, max);
            return currValue;
        }

        public DoubleRange setRandom(Random rnd)
        {
            random = rnd;
            return this;
        }

        public Double[] toArray(double step)
        {
            List<Double> range = new List<Double>();
            double v = min;
            while (v < max)
            {
                range.Add(v);
                v += step;
            }
            return range.ToArray();
        }

        
        public String toString()
        {
            return "DoubleRange: " + min + " -> " + max;
        }
    }
}
