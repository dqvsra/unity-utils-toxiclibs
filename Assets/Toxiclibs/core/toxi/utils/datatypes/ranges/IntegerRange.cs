﻿using System;
using System.Collections.Generic;
using toxi.math;

namespace toxi.utils.datatypes.ranges
{
    class IntegerRange
    {
        public static IntegerRange fromSamples(params int[] samples)
        {
            int min = int.MaxValue;
            int max = int.MinValue;
            foreach (int s in samples)
            {
                min = MathUtils.min(min, s);
                max = MathUtils.max(max, s);
            }
            return new IntegerRange(min, max);
        }

        public static IntegerRange fromSamples(List<int> samples)
        {
            int min = int.MaxValue;
            int max = int.MinValue;
            foreach (int s in samples)
            {
                min = MathUtils.min(min, s);
                max = MathUtils.max(max, s);
            }
            return new IntegerRange(min, max);
        }

        public int min, max;

        public int currValue;

        protected Random random = new Random();

        public IntegerRange() : this(0, 100)
        {
          
        }

        public IntegerRange(int min, int max)
        {
            // swap if necessary...
            if (min > max)
            {
                max ^= min;
                min ^= max;
                max ^= min;
            }
            this.min = min;
            this.max = max;
            this.currValue = min;
        }

        public int adjustCurrentBy(int val)
        {
            return setCurrent(currValue + val);
        }

        public IntegerRange copy()
        {
            IntegerRange range = new IntegerRange(min, max);
            range.currValue = currValue;
            range.random = random;
            return range;
        }

        /**
         * Returns the value at the normalized position
         * <code>(0.0 = min ... 1.0 = max-1)</code> within the range. Since the max
         * value is exclusive, the value returned for position 1.0 is the range max
         * value minus 1. Also note the given position is not being clipped to the
         * 0.0-1.0 interval, so when passing in values outside that interval will
         * produce out-of-range values too.
         * 
         * @param perc
         * @return value within the range
         */
        public int getAt(float perc)
        {
            return (int)(min + (max - min - 1) * perc);
        }

        public int getCurrent()
        {
            return currValue;
        }

        public int getMedian()
        {
            return (min + max) / 2;
        }

        public int getRange()
        {
            return max - min;
        }

        public bool isValueInRange(int val)
        {
            return val >= min && val < max;
        }

        public int pickRandom()
        {
            currValue = MathUtils.random(random, min, max);
            return currValue;
        }

        public IntegerRange seed(int seed)
        {
            random = new Random(seed);
            return this;
        }

        public int setCurrent(int val)
        {
            currValue = MathUtils.clip(val, min, max);
            return currValue;
        }

        public IntegerRange setRandom(Random rnd)
        {
            random = rnd;
            return this;
        }

        public int[] toArray()
        {
            int[] range = new int[max - min];
            for (int i = 0, j = min; j < max; i++, j++)
            {
                range[i] = j;
            }
            return range;
        }

        public String toString()
        {
            return "IntegerRange: " + min + " -> " + max;
        }
    }
}
