﻿using System;
using System.Collections.Generic;
using toxi.math;

namespace toxi.utils.datatypes.ranges
{
    class FloatRange
    {
        public static FloatRange fromSamples(params float[] samples)
        {
            float min = float.MaxValue;
            float max = float.MinValue;
            foreach (float s in samples)
            {
                min = MathUtils.min(min, s);
                max = MathUtils.max(max, s);
            }
            return new FloatRange(min, max);
        }

        public static FloatRange fromSamples(List<float> samples)
        {
            float min = float.MaxValue;
            float max = float.MinValue;
            foreach (float s in samples)
            {
                min = MathUtils.min(min, s);
                max = MathUtils.max(max, s);
            }
            return new FloatRange(min, max);
        }

        public float min, max;

        public float currValue;

        protected Random random = new Random();

        public FloatRange() : this(0f, 1f)
        {
            
        }

        public FloatRange(float min, float max)
        {
            // swap if necessary...
            if (min > max)
            {
                float t = max;
                max = min;
                min = t;
            }
            this.min = min;
            this.max = max;
            this.currValue = min;
        }

        public float adjustCurrentBy(float val)
        {
            return setCurrent(currValue + val);
        }

        public FloatRange copy()
        {
            FloatRange range = new FloatRange(min, max);
            range.currValue = currValue;
            range.random = random;
            return range;
        }

        /**
         * Returns the value at the normalized position <code>(0.0 = min ... 1.0 =
         * max-EPS)</code> within the range. Since the max value is exclusive, the
         * value returned for position 1.0 is the range max value minus
         * {@link MathUtils#EPS}. Also note the given position is not being clipped
         * to the 0.0-1.0 interval, so when passing in values outside that interval
         * will produce out-of-range values too.
         * 
         * @param perc
         * @return value within the range
         */
        public float getAt(float perc)
        {
            return min + (max - min - MathUtils.EPS) * perc;
        }

        public float getCurrent()
        {
            return currValue;
        }

        public float getMedian()
        {
            return (min + max) * 0.5f;
        }

        public float getRange()
        {
            return max - min;
        }

        public bool isValueInRange(float val)
        {
            return val >= min && val <= max;
        }

        public float pickRandom()
        {
            currValue = MathUtils.random(random, min, max);
            return currValue;
        }

        public FloatRange seed(int seed)
        {
            random = new Random(seed);
            return this;
        }

        public float setCurrent(float val)
        {
            currValue = MathUtils.clip(val, min, max);
            return currValue;
        }

        public FloatRange setRandom(Random rnd)
        {
            random = rnd;
            return this;
        }

        public float[] toArray(float step)
        {
            List<float> range = new List<float>();
            double v = min;
            while (v < max)
            {
                range.Add((float)v);
                v += step;
            }
            return range.ToArray();
        }

        public String toString()
        {
            return "FloatRange: " + min + " -> " + max;
        }
    }
}
