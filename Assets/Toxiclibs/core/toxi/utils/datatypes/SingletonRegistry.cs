﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace toxi.utils.datatypes
{

    /**
     * Implements a registry for dynamic singleton management. Use this registry
     * instead of using "new" to enforce singletons of any class with a visible
     * default constructor. The registry itself is implemented as singleton.
     */

    class SingletonRegistry
    {
        /**
     * The singleton instance of the registry itself.
     */
        public static SingletonRegistry REGISTRY = new SingletonRegistry();

        private static Dictionary<String, Object> map = new Dictionary<String, Object>();

        public static object GetNewObject(Type t)
        {
            try
            {
                return t.GetConstructor(new Type[] { }).Invoke(new object[] { });
            }
            catch
            {
                return null;
            }
        }

        /**
         * Creates or returns an instance of the class requested by name.
         * 
         * @param className
         * @return class singleton instance
         */
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static Object getInstanceOf(String className)
        {
            Object instance = map[className];
            if (instance != null)
            {
                return instance;
            }
            try
            {
                instance = GetNewObject(Type.GetType(className));
                map.Add(className, instance);
            }
            catch (Exception cnf)
            {
            }
            return instance;
        }

        /**
         * Alternative, more conventional accessor to the singleton instance of the
         * registry itself.
         * 
         * @return registry instance
         */
        public static SingletonRegistry getRegistry()
        {
            return REGISTRY;
        }

        protected SingletonRegistry()
        {
        }
    }
}
