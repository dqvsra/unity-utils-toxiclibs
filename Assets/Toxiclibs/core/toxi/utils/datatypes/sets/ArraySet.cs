﻿using System.Collections;

namespace toxi.utils.datatypes.sets
{
    /**
     * An ArrayList implementation of Set. An ArraySet is good for small sets; it
     * has less overhead than a HashSet or a TreeSet.
     * 
     * @author Paul Chew
     * 
     *         Created December 2007. For use with Voronoi/Delaunay applet.
     * 
     */
    class ArraySet
    {
        private ArrayList m_items; // Items of the set

        /**
         * Create an empty set (default initial capacity is 3).
         */
        public ArraySet(): this(3)
        {
        }

        /**
         * Create a set containing the m_items of the collection. Any duplicate m_items
         * are discarded.
         * 
         * @param collection
         *            the source for the m_items of the small set
         */
        public ArraySet(ICollection collection)
        {
            m_items = new ArrayList(collection.Count);
            foreach (var item in collection)
            {
                if (!m_items.Contains(item))
                {
                    m_items.Add(item);
                }
            }
        }

        /**
         * Create an empty set with the specified initial capacity.
         * 
         * @param initialCapacity
         *            the initial capacity
         */
        public ArraySet(int initialCapacity)
        {
            m_items = new ArrayList(initialCapacity);
        }
        
        public int Add<T>(T item)
        {
            if (m_items.Contains(item))
            {
                return -1;
            }
            return m_items.Add(item);
        }

        /**
         * True if any member of the collection is also in the ArraySet.
         * 
         * @param collection
         *            the Collection to check
         * @return true if any member of collection appears in this ArraySet
         */
        public bool containsAny(ICollection collection)
        {
            foreach (var item in collection)
            {
                if (m_items.Contains(item))
                {
                    return true;
                }
            }
            return false;
        }

        /**
         * Get the item at the specified index.
         * 
         * @param index
         *            where the item is located in the ListSet
         * @return the item at the specified index
         * @throws IndexOutOfBoundsException
         *             if the index is out of bounds
         */
        public object get(int index)
        {
            return m_items[index];
        }

        public IEnumerator iterator() {
            return m_items.GetEnumerator();
        }

        public int size()
        {
            return m_items.Count;
        }
    }
}
