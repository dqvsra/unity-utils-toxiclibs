﻿using System;
using System.Collections;
using System.Collections.Generic;
using toxi.math;

namespace toxi.utils.datatypes.sets
{
    class GenericSet<T> : IEquatable<T>
    {
        public bool Equals(T other)
        {
            throw new NotImplementedException();
        }

        protected List<T> items;
        protected int currID = -1;
        protected T current;

        protected Random random = new Random();

        public GenericSet(ICollection<T> items)
        {
            this.items = new List<T>(items);
            pickRandom();
        }

        public GenericSet(params T[] obj)
        {
            int length = obj.Length;
            items = new List<T>(length);
            for (int i = 0; i < length; i++)
            {
                items.Add(obj[i]);
            }
            if (items.Count > 0)
            {
                pickRandom();
            }
        }

        public int add(T obj)
        {
            items.Add(obj);
            if (items.Count == 1)
            {
                pickRandom();
            }
            return items.Count;
        }

        public void addAll(ICollection<T> coll)
        {
            items.AddRange(coll);
        }

        public void clear()
        {
            items.Clear();
        }

        public bool contains(T obj)
        {
            return items.Contains(obj);
        }

        public GenericSet<T> copy()
        {
            GenericSet<T> set = new GenericSet<T>(items);
            set.current = current;
            set.currID = currID;
            set.random = random;
            return set;
        }

        public T getCurrent()
        {
            return current;
        }

        public List<T> getItems()
        {
            return items;
        }

        public IEnumerator iterator()
        {
            return items.GetEnumerator();
        }

        public T pickRandom()
        {
            currID = MathUtils.random(random, items.Count);
            current = items[currID];
            return current;
        }

        public T pickRandomUnique()
        {
            int size = items.Count;
            if (size > 1)
            {
                int newID = currID;
                while (newID == currID)
                {
                    newID = MathUtils.random(random, size);
                }
                currID = newID;
            }
            else
            {
                currID = 0;
            }
            current = items[currID];
            return current;
        }

        public GenericSet<T> seed(int seed)
        {
            random = new Random(seed);
            return this;
        }

        public void setRandom(Random rnd)
        {
            random = rnd;
        }

        public int size()
        {
            return items.Count;
        }
    }
}
