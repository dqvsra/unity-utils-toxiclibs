﻿using System;
using toxi.math;

namespace toxi.utils.datatypes.sets
{
    class IntegerSet
    {
        public int[] items;
        public int currID = -1;
        public int current;

        private Random random = new Random();

        public IntegerSet(int[] items)
        {
            this.items = items;
            pickRandom();
        }

        //public IntegerSet(params int[] input)
        //{
        //    int counter = input.Length;
        //    if (counter > 0)
        //    {
        //        this.items = new int[counter];
        //        while(counter-- > 0)
        //        {
        //            this.items[counter] = input[counter];
        //        }
        //        pickRandom();
        //    }
        //    else
        //    {
        //        //throw new Exception("IntegerSet can't be zero");
        //    }
        //}

        public bool contains(int value)
        {
            int counter = items.Length;
            while (counter-- > 0)
            {
                if (items[counter] == value)
                {
                    return true;
                }
            }
            return false;
        }

        public int getCurrent()
        {
            return current;
        }

        public int pickRandom()
        {
            currID = MathUtils.random(random, items.Length);
            current = items[currID];
            return current;
        }

        public int pickRandomUnique()
        {
            int length = items.Length;
            if (length > 1)
            {
                int newID = currID;
                while (newID == currID)
                {
                    newID = MathUtils.random(random, length);
                }
                currID = newID;
            }
            else
            {
                currID = 0;
            }
            current = items[currID];
            return current;
        }

        public IntegerSet seed(int seed)
        {
            random = new Random(seed);
            return this;
        }

        public void setRandom(Random rnd)
        {
            random = rnd;
        }
    }
}
