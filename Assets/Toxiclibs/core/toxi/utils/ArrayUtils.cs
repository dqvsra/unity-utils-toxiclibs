﻿using System;
using System.Collections;
using System.Text;

namespace toxi.utils
{
    class ArrayUtils
    {
        /**
         * Adds all array elements to the given collection of the same type.
         * 
         * @param <T>
         * @param array array
         * @param collection existing collection or null (to create a new {@link ArrayList automatically)
         */
        public static void addArrayToCollection<T>(T[] array, ArrayList collection)
        {
            if (collection == null)
            {
                collection = new ArrayList();
            }
            foreach (T element in array)
            {
                collection.Add(element);
            }
        }

        /**
         * Converts the generic array into an {@link ArrayList} of the same type.
         * 
         * @param array
         * @return array list version
         */
        public static ArrayList arrayToList<T>(T[] array)
        {
            ArrayList list = new ArrayList(array.Length);
            foreach (T element in array)
            {
                list.Add(element);
            }
            return list;
        }

        /**
         * Creates a normalized version of the values of the given int[] array.
         * Supports packed integers (e.g. ARGB data) by allowing to specify a
         * bitshift amount & bitmask, e.g. do this to get the normalized
         * representation of the red channel of an ARGB array:
         * 
         * <pre>
         * 
         * // use 16 bits as shift offset for accessing red channel
         * float[] red = ArrayUtil.getAsNormalizedFloatArray(argbPixels, 16, 255, 255);
         * </pre>
         * 
         * @param source source data
         * @param bits number of bits to right shift each value
         * @param mask bitmask to apply after bitshifting
         * @param peak peak value (in the source domain) to normalize against
         * @param target peak of the normalized values
         * @return normalized values
         */
        public static float[] getAsNormalizedFloatArray(int[] source, int bits,
                int mask, int peak, float target)
        {
            float invPeak = target / peak;
            int sourceLength = source.Length;
            float[] normalized = new float[sourceLength];
            for (int i = 0, value; i < sourceLength; i++)
            {
                value = source[i];
                if (bits > 0)
                {
                    value >>= bits;
                }
                value &= mask;
                normalized[i] = value * invPeak;
            }
            return normalized;
        }

        /**
         * Returns the index of the element where the given value is found in the
         * array.
         * 
         * @param needle number to find
         * @param stack array to search
         * @param maxLen number of elements to search
         * @return array index or -1 if value couldn't be found in array
         */
        public static int indexInArray(float needle, float[] stack, int? maxLen = null)
        {
            float value;
            if (maxLen == null) maxLen = stack.Length;
            for (int i = 0; i < maxLen; i++)
            {
                value = stack[i];
                if (value.Equals(needle))
                {
                    return i;
                }
            }
            return -1;
        }

        /**
         * Normalizes the values in the given array to the new absolute target
         * value. The original values are overridden.
         * 
         * @param buffer
         *            array
         * @param peak
         *            current peak in the source domain
         * @param target
         *            new peak in the target domain
         * @return normalized array
         */
        public static float[] normalizeFloatArray(float[] buffer, float peak,
                float target)
        {
            float invPeak = target / peak;
            int counter = buffer.Length;
            float bufferValue = 0f;
            while (counter-- > 0)
            {
                bufferValue = buffer[counter];
                buffer[counter] = bufferValue * invPeak;
            }
            return buffer;
        }

        /**
         * Reverses the item order of the supplied byte array.
         * 
         * @param array
         */
        public static void reverse(byte[] array)
        {
            int len = array.Length - 1;
            int counter = array.Length / 2;
            byte tempValue = 0;
            int position = 0;
            while (counter-- > 0)
            {
                tempValue = array[counter];
                position = len - counter;
                array[counter] = array[position];
                array[position] = tempValue;
            }
        }

        /**
         * Reverses the item order of the supplied T array.
         * 
         * @param array
         */
        public static void reverse<T>(T[] array)
        {
            int len = array.Length - 1;
            int counter = array.Length / 2;
            T tempValue;
            int position = 0;
            while (counter-- > 0)
            {
                tempValue = array[counter];
                position = len - counter;
                array[counter] = array[position];
                array[position] = tempValue;
            }
        }

        /**
         * Reverses the item order of the supplied char array.
         * 
         * @param array
         */
        public static void reverse(char[] array)
        {
            int len = array.Length - 1;
            int counter = array.Length / 2;
            char tempValue = ' ';
            int position = 0;
            while (counter-- > 0)
            {
                tempValue = array[counter];
                position = len - counter;
                array[counter] = array[position];
                array[position] = tempValue;
            }
        }

        /**
         * Reverses the item order of the supplied int array.
         * 
         * @param array
         */
        public static void reverse(int[] array)
        {
            int len = array.Length - 1;
            int counter = array.Length / 2;
            int tempValue = 0;
            int position = 0;
            while (counter-- > 0)
            {
                tempValue = array[counter];
                position = len - counter;
                array[counter] = array[position];
                array[position] = tempValue;
            }
        }

        /**
         * Rearranges the array m_items in random order using the default
         * java.util.Random generator. Operation is in-place, no copy is created.
         * 
         * @param array
         */
        public static void shuffle<T>(T[] array)
        {
            shuffle(array, new Random());
        }

        /**
         * Rearranges the array m_items in random order using the given RNG. Operation
         * is in-place, no copy is created.
         * 
         * @param array
         * @param rnd
         */
        public static void shuffle<T>(T[] array, Random rnd)
        {
            int N = array.Length;
            T swapValue;
            int position = 0;
            int randomPos = 0;
            for (int i = 0; i < N; i++)
            {
                position = N - i;
                randomPos = i + rnd.Next(position); // between i and N-1
                swapValue = array[i];
                array[i] = array[randomPos];
                array[randomPos] = swapValue;
            }
        }

        public static String toString<T>(T[] array)
        {
            StringBuilder s = new StringBuilder();
            s.Append('{');
            int length = array.Length;
            int max = length - 1;
            for (int i = 0; i < length; i++)
            {
                s.Append(array[i]);
                if (i < max)
                {
                    s.Append(',');
                }
            }
            return s.Append('}').ToString();
        }
    }
}
